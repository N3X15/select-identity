# Select Identity

An identity management system for Git, allowing for quick, consistent application of aliases. Useful for keeping your personal and professional lives separate.

Not perfect, but allows for some semblance of plausible deniability.

## Installation

1. Install Select Identity to your system, somewhere accessible. 
```shell
# Note: If you're on Linux, you may need to use pip3 instead of pip.
sudo -H pip install git+https://gitlab.com/N3X15/select-identity.git ~/select-identity
```
1. Copy `identities.example.yml` to `~/.git-identity.yml` (`%HOME%\\.git-identity.yml` on Windows) and edit to taste.

## Use

Assuming `N3X15` is your desired identity:

### Selecting your identity
This stores your desired identity in your local git config and configures the repo to use the correct email, user name, and any signing keys.

It also adds the PUSH.sh, PULL.sh, and FETCH.sh scripts and adds them to `.git/info/exclude` so they won't interfere with anything.

```shell
$ git identity select N3X15
```
```
05/23/2022 07:15:43 PM [INFO    ]: Checking if git exists...
05/23/2022 07:15:43 PM [INFO    ]:   Found: /usr/lib/git-core/git
05/23/2022 07:15:43 PM [INFO    ]: Determining available remotes...
05/23/2022 07:15:43 PM [INFO    ]: $ /usr/lib/git-core/git config --local user.email nexisentertainment@gmail.com
05/23/2022 07:15:43 PM [INFO    ]: $ /usr/lib/git-core/git config --local user.name 'Rob Nelson'
05/23/2022 07:15:43 PM [INFO    ]: $ /usr/lib/git-core/git config --local user.selected-identity N3X15
05/23/2022 07:15:43 PM [INFO    ]: $ /usr/lib/git-core/git config --local core.sshCommand 'ssh -i "/home/nexis/.ssh/id_rsa" -F /dev/null'
05/23/2022 07:15:43 PM [INFO    ]: Cleaning old files...
05/23/2022 07:15:43 PM [INFO    ]: $ pre-commit install
pre-commit installed at .git/hooks/pre-commit
05/23/2022 07:15:44 PM [INFO    ]: Generating /home/nexis/select-identity/.selid-hooks/check-identity from template /home/nexis/.local/lib/python3.8/site-packages/git_identity/templates/universal/hooks/check-identity.py...
05/23/2022 07:15:44 PM [INFO    ]: Making /home/nexis/select-identity/.selid-hooks/check-identity executable (chmod +x)...
```


### Local commands

All the *local* repo commands should work as expected, and will use the stored info.

### Man Pages
`man` pages are currently available to eventually supply `git help`, but due to a bug in setuptools and disttools, installing these must be done manually.

The same information is available via `git identity [command] --help`, anyway.

## License

This project is available under the [MIT Open Source License](./LICENSE).