import os, jinja2
from ruamel.yaml import YAML
from buildtools import os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.docs import RonnBuildTarget
from buildtools.maestro.jinja2 import Jinja2BuildTarget, j_cmd

yaml = YAML(typ='rt')
jenv = jinja2.Environment(loader=jinja2.FileSystemLoader('.'),
                          extensions=['jinja2.ext.do'],
                          autoescape=False)

jenv.globals.update(cmd=j_cmd)
MAN_SRC = 'man-src'
MAN_OUT = 'man'
DOC_SRC = 'doc-src'
DOC_OUT = 'doc'
GITHUB = 'https://gitlab.com/N3X15/select_identity'

RONN = os_utils.assertWhich('ronn')
COMMANDS = {}

with open(os.path.join(MAN_SRC, 'commands.yml'), 'r') as f:
    COMMANDS = yaml.load(f)

jenv.globals.update(COMMANDS=COMMANDS, GITHUB=GITHUB)

bm = BuildMaestro()
for filename in os.listdir('man-src'):
    if not filename.endswith('.tmpl.md'):
        continue
    parts = filename.split('.')
    basename = os.path.basename(filename)
    section = None
    intermediate = bm.add(Jinja2BuildTarget(source_filename=os.path.join(MAN_SRC, filename),
                                            dest_filename=os.path.join(MAN_OUT, f'{basename[:-8]}'),
                                            jenv=jenv))
    if len(parts) == 4:
        basename, section, _, _ = parts
        bm.add(RonnBuildTarget(markdown_filename=intermediate.target,
                               section=int(section),
                               dependencies=[intermediate.target],
                               ronn_executable=RONN))
bm.add(Jinja2BuildTarget(source_filename=os.path.join(DOC_SRC, 'README.tmpl.md'),
                         dest_filename=os.path.join(f'README.md'),
                         jenv=jenv))
bm.as_app()
