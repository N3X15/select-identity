{% macro _an_argument(start, end, argnames, argmeta='') -%}
  {{start}}{%- if argmeta == '' -%}
  **{{ argnames|join('**|**') }}**
  {%- else -%}
  **{{ argnames|join('**|**') }}** {{argmeta}}
  {%- endif -%}{{end}}
{%- endmacro %}
{% macro optional_argument(argnames, argmeta='') -%}
  {{ _an_argument('[',']',argnames,argmeta) }}
{%- endmacro %}
{% macro required_argument(argnames, argmeta='') -%}
  {{ _an_argument('&lt;','&gt;',argnames,argmeta) }}
{%- endmacro %}
{%- macro meta(name) -%}
  <{{ name }}>
{%- endmacro -%}
{% macro _a_recursive_meta(name) -%}
  {{ meta(name) }} [{{ meta(name) }} ...]
{%- endmacro %}
{% macro required_recursive_argument(argnames, meta) -%}
  {{ required_argument(argnames, _a_recursive_meta(name))}}
{%- endmacro %}
{% macro optional_recursive_argument(argnames, meta) -%}
  {{ optional_argument(argnames, _a_recursive_meta(name))}}
{%- endmacro -%}
{%- macro def_argument_start(argnames, val='') %}
  * `{{ argnames|join('`, `') }}`
  {%- if val != '' -%}
    ={{- val -}}
  {%- endif -%}
  :
{%- endmacro -%}
{%- macro def_argument_end() -%}
{%- endmacro -%}
