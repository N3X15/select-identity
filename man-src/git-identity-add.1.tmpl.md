{%- from 'man-src/macros.jinja.md' import optional_argument, required_argument, meta, def_argument_start, def_argument_end -%}
git-identity-add(1) -- Add a new identity to git-identity.
===============================================

## SYNOPSIS

`git-identity` `add` {{optional_argument(['-h'])}} {{optional_argument(['--signing-key'], meta('SIGNING_KEY'))}} {{ required_argument(['identity']) }} {{ required_argument(['name']) }} {{ required_argument(['email']) }} {{ required_argument(['privkey']) }} {{ required_argument(['pubkey']) }}

## DESCRIPTION

  Dumps a list of all identities available for use in git-identity.

## OPTIONS
{{ def_argument_start(['identity']) }}
  The new identity's ID.
{{ def_argument_end() -}}

{{ def_argument_start(['name']) }}
  The new identity's name.
{{ def_argument_end() -}}

{{ def_argument_start(['email']) }}
  The new identity's e-mail address.
{{ def_argument_end() -}}

{{ def_argument_start(['privkey']) }}
  The new identity's private key.
{{ def_argument_end() -}}

{{ def_argument_start(['pubkey']) }}
  The new identity's public key.
{{ def_argument_end() -}}

{{ def_argument_start(['-h', '--help']) }}
  Show a help message and exit.
{{ def_argument_end() -}}

{{ def_argument_start(['--signing-key'], meta('SIGNING_KEY')) }}
  The path to a GPG signing key.
{{ def_argument_end() }}

## AUTHOR

Written by Rob Nelson &lt;nexisentertainment@gmail.com&gt;

## REPORTING BUGS

&lt;<{{ GITHUB }}/issues>&gt;

## SEE ALSO

&lt;<{{ GITHUB }}>&gt;
