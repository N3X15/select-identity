{%- from 'man-src/macros.jinja.md' import optional_argument, required_argument, meta, def_argument_start, def_argument_end -%}
git-identity-select(1) -- Select an identity for git to use.
===============================================

## SYNOPSIS
`git-identity` `clone` {{ optional_argument(['-h']) }} {{ optional_argument(['--ssh-verbose']) }} {{ required_argument(['--as'], meta('IDENTITY')) }} {{ optional_argument(['--no-scripts']) }} {{ optional_argument(['--no-hooks']) }} ...

## DESCRIPTION

  Provides various interfaces to permit identity management for git.

## OPTIONS
{{def_argument_start(['-h', '--help'])}}
  show this help message and exit
{{def_argument_end()-}}

{{def_argument_start(['-v', '--ssh-verbose'])}}
  Tell SSH to be more verbose.
{{def_argument_end()-}}

{{def_argument_start(['--as'], meta('IDENTITY'))}}
  Clone repo using the given identity.
{{def_argument_end()-}}

{{def_argument_start(['--no-scripts'])}}
  Disable PULL/PUSH/etc shell script generation.
{{def_argument_end()-}}

{{def_argument_start(['--no-hooks'])}}
  Disable pre-commit hook generation.
{{def_argument_end()}}

## AUTHOR

Written by Rob Nelson &lt;nexisentertainment@gmail.com&gt;

## REPORTING BUGS

&lt;<{{ GITHUB }}/issues>&gt;

## SEE ALSO

&lt;<{{ GITHUB }}>&gt;
