{%- from 'man-src/macros.jinja.md' import optional_argument, required_argument, meta, def_argument_start, def_argument_end -%}
git-identity(1) -- Identity management for git
===============================================

## SYNOPSIS

`git-identity` {{optional_argument(['-h'])}} {select,pull,fetch,push,clone}

## DESCRIPTION

  Provides various interfaces to permit identity management for git.

## OPTIONS
{{ def_argument_start(['-h', '--help']) }}
  Show this help. This option can also be used for any of the identities commands.
{{ def_argument_end() }}

## COMMANDS
{%- for command, description in COMMANDS.items() %}
  - **{{command}}(1)** {{description}}
{%- endfor %}

## AUTHOR

Written by Rob Nelson &lt;nexisentertainment@gmail.com&gt;

## REPORTING BUGS

&lt;<{{ GITHUB }}/issues>&gt;

## SEE ALSO

&lt;<{{ GITHUB }}>&gt;
