{%- from 'man-src/macros.jinja.md' import optional_argument, required_argument, meta, def_argument_start, def_argument_end -%}
git-identity-list(1) -- List all configured identities
===============================================

## SYNOPSIS

`git-identity` `list` {{optional_argument(['-h'])}}

## DESCRIPTION

  Dumps a list of all identities available for use in git-identity.

## OPTIONS
{{ def_argument_start(['-h', '--help']) }}
  Show a help message and exit.
{{ def_argument_end() }}

## AUTHOR

Written by Rob Nelson &lt;nexisentertainment@gmail.com&gt;

## REPORTING BUGS

&lt;<{{ GITHUB }}/issues>&gt;

## SEE ALSO

&lt;<{{ GITHUB }}>&gt;
