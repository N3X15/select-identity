{%- from 'man-src/macros.jinja.md' import optional_argument, required_argument, meta, def_argument_start, def_argument_end -%}
git-identity-push(1) -- Push changes using the given identity.
===============================================

## SYNOPSIS

`git-identity` `push` {{ optional_argument(['-h','--help']) }} {{ optional_argument(['--ssh-verbose']) }} ...

## DESCRIPTION

  Perform a `git push` using the provided identity.

## OPTIONS
{{ def_argument_start(['-h', '--help']) }}
  Show this help message and exit
{{ def_argument_end() -}}

{{ def_argument_start(['--ssh-verbose', '-v']) }}
  Tell SSH to be more verbose.
{{ def_argument_end() }}

## AUTHOR

Written by Rob Nelson &lt;nexisentertainment@gmail.com&gt;

## REPORTING BUGS

&lt;<{{ GITHUB }}/issues>&gt;

## SEE ALSO

&lt;<{{ GITHUB }}>&gt;
