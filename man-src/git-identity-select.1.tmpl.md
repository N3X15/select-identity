{%- from 'man-src/macros.jinja.md' import optional_argument, required_argument, meta, def_argument_start, def_argument_end -%}
git-identity-select(1) -- Select an identity for git to use.
===============================================

## SYNOPSIS

`git-identity` `select` {{ optional_argument(['-h', '--help']) }} {{ optional_argument(['-R','--remote'], meta('REMOTE')) }} {{ optional_argument(['-S','--no-scripts']) }} {{ optional_argument(['-H','--no-hooks']) }} {{ meta('identity') }}

## DESCRIPTION

  Provides various interfaces to permit identity management for git.

## OPTIONS
{{ def_argument_start(['identity']) }}
  Identity to select from .git-identities
{{ def_argument_end() -}}

{{ def_argument_start(['-h', '--help']) }}
  Show this help message and exit.
{{ def_argument_end() -}}

{{ def_argument_start(['--remote', '-R'], meta('REMOTE')) }}
  Origins to use.
{{ def_argument_end() -}}

{{ def_argument_start(['--no-scripts, -S']) }}
  Disable PULL/PUSH/etc shell script generation.
{{ def_argument_end() -}}

{{ def_argument_start(['--no-hooks, -H']) }}
  Disable pre-commit hook generation.
{{ def_argument_end() }}

## AUTHOR

Written by Rob Nelson &lt;nexisentertainment@gmail.com&gt;

## REPORTING BUGS

&lt;<{{ GITHUB }}/issues>&gt;

## SEE ALSO

&lt;<{{ GITHUB }}>&gt;
