git-identity(1) -- Identity management for git
===============================================

## SYNOPSIS

`git-identity` [**-h**] {select,pull,fetch,push,clone}

## DESCRIPTION

  Provides various interfaces to permit identity management for git.

## OPTIONS

  * `-h`, `--help`:
  Show this help. This option can also be used for any of the identities commands.


## COMMANDS
  - **git-identity-add(1)** Add identity to git-identity.
  - **git-identity-clone(1)** Clone a repository using the given identity.
  - **git-identity-fetch(1)** Fetch changes from a repository.
  - **git-identity-list(1)** List all configured identities.
  - **git-identity-pull(1)** Pull changes from a repository into the checked out branch.
  - **git-identity-push(1)** Push local changes to a repository.
  - **git-identity-select(1)** Select which identity to use with the current git repository.

## AUTHOR

Written by Rob Nelson &lt;nexisentertainment@gmail.com&gt;

## REPORTING BUGS

&lt;<https://gitlab.com/N3X15/select_identity/issues>&gt;

## SEE ALSO

&lt;<https://gitlab.com/N3X15/select_identity>&gt;

[SYNOPSIS]: #SYNOPSIS "SYNOPSIS"
[DESCRIPTION]: #DESCRIPTION "DESCRIPTION"
[OPTIONS]: #OPTIONS "OPTIONS"
[COMMANDS]: #COMMANDS "COMMANDS"
[AUTHOR]: #AUTHOR "AUTHOR"
[REPORTING BUGS]: #REPORTING-BUGS "REPORTING BUGS"
[SEE ALSO]: #SEE-ALSO "SEE ALSO"


[git-identity.1]: git-identity.1.html
