git-identity-push(1) -- Push changes using the given identity.
===============================================

## SYNOPSIS

`git-identity` `push` [**-h**|**--help**] [**--ssh-verbose**] ...

## DESCRIPTION

  Perform a `git push` using the provided identity.

## OPTIONS

  * `-h`, `--help`:
  Show this help message and exit

  * `--ssh-verbose`, `-v`:
  Tell SSH to be more verbose.


## AUTHOR

Written by Rob Nelson &lt;nexisentertainment@gmail.com&gt;

## REPORTING BUGS

&lt;<https://gitlab.com/N3X15/select_identity/issues>&gt;

## SEE ALSO

&lt;<https://gitlab.com/N3X15/select_identity>&gt;

[SYNOPSIS]: #SYNOPSIS "SYNOPSIS"
[DESCRIPTION]: #DESCRIPTION "DESCRIPTION"
[OPTIONS]: #OPTIONS "OPTIONS"
[AUTHOR]: #AUTHOR "AUTHOR"
[REPORTING BUGS]: #REPORTING-BUGS "REPORTING BUGS"
[SEE ALSO]: #SEE-ALSO "SEE ALSO"


[git-identity-push.1]: git-identity-push.1.html
