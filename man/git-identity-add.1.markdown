git-identity-add(1) -- Add a new identity to git-identity.
===============================================

## SYNOPSIS

`git-identity` `add` [**-h**] [**--signing-key** <var>SIGNING_KEY</var>] &lt;**identity**&gt; &lt;**name**&gt; &lt;**email**&gt; &lt;**privkey**&gt; &lt;**pubkey**&gt;

## DESCRIPTION

  Dumps a list of all identities available for use in git-identity.

## OPTIONS

  * `identity`:
  The new identity's ID.

  * `name`:
  The new identity's name.

  * `email`:
  The new identity's e-mail address.

  * `privkey`:
  The new identity's private key.

  * `pubkey`:
  The new identity's public key.

  * `-h`, `--help`:
  Show a help message and exit.

  * `--signing-key`=<var>SIGNING_KEY</var>:
  The path to a GPG signing key.


## AUTHOR

Written by Rob Nelson &lt;nexisentertainment@gmail.com&gt;

## REPORTING BUGS

&lt;<https://gitlab.com/N3X15/select_identity/issues>&gt;

## SEE ALSO

&lt;<https://gitlab.com/N3X15/select_identity>&gt;

[SYNOPSIS]: #SYNOPSIS "SYNOPSIS"
[DESCRIPTION]: #DESCRIPTION "DESCRIPTION"
[OPTIONS]: #OPTIONS "OPTIONS"
[AUTHOR]: #AUTHOR "AUTHOR"
[REPORTING BUGS]: #REPORTING-BUGS "REPORTING BUGS"
[SEE ALSO]: #SEE-ALSO "SEE ALSO"


[git-identity-add.1]: git-identity-add.1.html
