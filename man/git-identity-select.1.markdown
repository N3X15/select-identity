git-identity-select(1) -- Select an identity for git to use.
===============================================

## SYNOPSIS

`git-identity` `select` [**-h**|**--help**] [**-R**|**--remote** <var>REMOTE</var>] [**-S**|**--no-scripts**] [**-H**|**--no-hooks**] <var>identity</var>

## DESCRIPTION

  Provides various interfaces to permit identity management for git.

## OPTIONS

  * `identity`:
  Identity to select from .git-identities

  * `-h`, `--help`:
  Show this help message and exit.

  * `--remote`, `-R`=<var>REMOTE</var>:
  Origins to use.

  * `--no-scripts, -S`:
  Disable PULL/PUSH/etc shell script generation.

  * `--no-hooks, -H`:
  Disable pre-commit hook generation.


## AUTHOR

Written by Rob Nelson &lt;nexisentertainment@gmail.com&gt;

## REPORTING BUGS

&lt;<https://gitlab.com/N3X15/select_identity/issues>&gt;

## SEE ALSO

&lt;<https://gitlab.com/N3X15/select_identity>&gt;

[SYNOPSIS]: #SYNOPSIS "SYNOPSIS"
[DESCRIPTION]: #DESCRIPTION "DESCRIPTION"
[OPTIONS]: #OPTIONS "OPTIONS"
[AUTHOR]: #AUTHOR "AUTHOR"
[REPORTING BUGS]: #REPORTING-BUGS "REPORTING BUGS"
[SEE ALSO]: #SEE-ALSO "SEE ALSO"


[git-identity-select.1]: git-identity-select.1.html
