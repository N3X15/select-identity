git-identity-select(1) -- Select an identity for git to use.
===============================================

## SYNOPSIS
`git-identity` `clone` [**-h**] [**--ssh-verbose**] &lt;**--as** <var>IDENTITY</var>&gt; [**--no-scripts**] [**--no-hooks**] ...

## DESCRIPTION

  Provides various interfaces to permit identity management for git.

## OPTIONS

  * `-h`, `--help`:
  show this help message and exit

  * `-v`, `--ssh-verbose`:
  Tell SSH to be more verbose.

  * `--as`=<var>IDENTITY</var>:
  Clone repo using the given identity.

  * `--no-scripts`:
  Disable PULL/PUSH/etc shell script generation.

  * `--no-hooks`:
  Disable pre-commit hook generation.


## AUTHOR

Written by Rob Nelson &lt;nexisentertainment@gmail.com&gt;

## REPORTING BUGS

&lt;<https://gitlab.com/N3X15/select_identity/issues>&gt;

## SEE ALSO

&lt;<https://gitlab.com/N3X15/select_identity>&gt;

[SYNOPSIS]: #SYNOPSIS "SYNOPSIS"
[DESCRIPTION]: #DESCRIPTION "DESCRIPTION"
[OPTIONS]: #OPTIONS "OPTIONS"
[AUTHOR]: #AUTHOR "AUTHOR"
[REPORTING BUGS]: #REPORTING-BUGS "REPORTING BUGS"
[SEE ALSO]: #SEE-ALSO "SEE ALSO"


[git-identity-clone.1]: git-identity-clone.1.html
