git-identity-list(1) -- List all configured identities
===============================================

## SYNOPSIS

`git-identity` `list` [**-h**]

## DESCRIPTION

  Dumps a list of all identities available for use in git-identity.

## OPTIONS

  * `-h`, `--help`:
  Show a help message and exit.


## AUTHOR

Written by Rob Nelson &lt;nexisentertainment@gmail.com&gt;

## REPORTING BUGS

&lt;<https://gitlab.com/N3X15/select_identity/issues>&gt;

## SEE ALSO

&lt;<https://gitlab.com/N3X15/select_identity>&gt;

[SYNOPSIS]: #SYNOPSIS "SYNOPSIS"
[DESCRIPTION]: #DESCRIPTION "DESCRIPTION"
[OPTIONS]: #OPTIONS "OPTIONS"
[AUTHOR]: #AUTHOR "AUTHOR"
[REPORTING BUGS]: #REPORTING-BUGS "REPORTING BUGS"
[SEE ALSO]: #SEE-ALSO "SEE ALSO"


[git-identity-list.1]: git-identity-list.1.html
