from typing import Dict
import libcst


class StringReplacer(libcst.CSTTransformer):
    def __init__(self) -> None:
        self.replacements: Dict[str, str] = {}

    def addReplacement(self, needle: str, replacement: str) -> None:
        self.replacements[needle] = replacement

    def leave_SimpleString(
        self, original: libcst.SimpleString, updated: libcst.SimpleString
    ) -> libcst.BaseExpression:
        if original.value in self.replacements.keys():
            return updated.with_changes(value=self.replacements[original.value])
        return updated
